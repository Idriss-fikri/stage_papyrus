import torch

import torchvision

import numpy as np

import matplotlib.pyplot as plt

import math

from torchvision import models

import torch.nn.functional as F

from torch import nn

class SamePad2d(nn.Module):

   def __init__(self, kernel_size, stride):

       super(SamePad2d, self).__init__()

       self.kernel_size = torch.nn.modules.utils._pair(kernel_size)

       self.stride = torch.nn.modules.utils._pair(stride)

   def forward(self, input):

       in_width = input.size()[2]

       in_height = input.size()[3]

       out_width = math.ceil(float(in_width) / float(self.stride[0]))

       out_height = math.ceil(float(in_height) / float(self.stride[1]))

       pad_along_width = ((out_width - 1) * self.stride[0] + self.kernel_size[0] - in_width)

       pad_along_height = ((out_height - 1) * self.stride[1] + self.kernel_size[1] - in_height)

       pad_left = math.floor(pad_along_width / 2)

       pad_top = math.floor(pad_along_height / 2)

       pad_right = pad_along_width - pad_left

       pad_bottom = pad_along_height - pad_top

       return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)

class papy_s_net_branch(torch.nn.Module):

   def __init__(self, num_classes):

       super().__init__()

       self.padding = SamePad2d(kernel_size=3, stride=1)

       self.block_1 = torch.nn.Sequential(

               torch.nn.Conv2d(

                   in_channels=3,out_channels=64,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.Conv2d(

                   in_channels=64, out_channels=64,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.MaxPool2d(kernel_size=(2, 2),

                                  stride=(2, 2))

       )

      

       self.block_2 = torch.nn.Sequential(

               torch.nn.Conv2d(

                   in_channels=64,out_channels=128,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.Conv2d(

                   in_channels=128,out_channels=128,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.MaxPool2d(kernel_size=(2, 2),

                                  stride=(2, 2))

       )

       self.block_3 = torch.nn.Sequential(        

               torch.nn.Conv2d(

                   in_channels=128,out_channels=256,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.Conv2d(

                   in_channels=256,out_channels=256,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),        

               torch.nn.Conv2d(

                   in_channels=256,out_channels=256,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),

               torch.nn.MaxPool2d(kernel_size=(2, 2),

                                  stride=(2, 2))

       )

                

       self.block_4 = torch.nn.Sequential(  

               torch.nn.Conv2d(

                   in_channels=256,out_channels=512,kernel_size=(3, 3),stride=(1, 1),padding=1),  # ! typo

               torch.nn.ReLU(),        

               torch.nn.Conv2d(

                   in_channels=512,out_channels=512,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),        

               torch.nn.Conv2d(

                   in_channels=512, out_channels=512, kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),            

               torch.nn.MaxPool2d(kernel_size=(2, 2),

                                  stride=(2, 2))

       )

       self.block_5 = torch.nn.Sequential(

               torch.nn.Conv2d(

                   in_channels=512, out_channels=512,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),            

               torch.nn.Conv2d(

                   in_channels=512,out_channels=512,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),            

               torch.nn.Conv2d(

                   in_channels=512,out_channels=512,kernel_size=(3, 3),stride=(1, 1),padding=1),

               torch.nn.ReLU(),    

               torch.nn.MaxPool2d(kernel_size=(2, 2),

                                  stride=(2, 2))            

       )

       height, width = 3, 3 ## you may want to change that depending on the input image size

       self.classifier = torch.nn.Sequential(

           torch.nn.Linear(512*height*width, 4096),

           torch.nn.ReLU(True),

           torch.nn.Dropout(p=0.5),

           torch.nn.Linear(4096, 4096),

           torch.nn.ReLU(True),

           torch.nn.Dropout(p=0.5),

           torch.nn.Linear(4096, num_classes),

       )

          

       for m in self.modules():

           if isinstance(m, torch.torch.nn.Conv2d) or isinstance(m, torch.torch.nn.Linear):

               torch.nn.init.kaiming_uniform_(m.weight, mode='fan_in', nonlinearity='relu')

               if m.bias is not None:

                   m.bias.detach().zero_()

                  

       self.avgpool = torch.nn.AdaptiveAvgPool2d((height, width))

              

   def forward(self, x):

       x = self.block_1(x)

       x = self.block_2(x)

       x = self.block_3(x)

       x = self.block_4(x)

       x = self.block_5(x)

       x = self.avgpool(x)

       x = x.view(x.size(0), -1) # flatten

      

       logits = self.classifier(x)

       #probas = F.softmax(logits, dim=1)

       return logits

class Identity(nn.Module):

   def __init__(self):

       super(Identity, self).__init__()

   def forward(self, x):

       return x

class Siamese(nn.Module):

   def __init__(self, network_type, dropout=None):

       super(Siamese, self).__init__()

       assert network_type in ["vgg", "papy", "resnet50"]

       if network_type == 'resnet50':

           self.feature_extraction = models.resnet50(pretrained=True) # xe cal the predifinedt models

           self.feature_extraction.fc = Identity()

           self.fc0 = nn.Linear(2048, 512)

       elif network_type == 'vgg':

           self.feature_extraction = models.vgg16(pretrained=True) # also we call the predfifned model

           self.feature_extraction.classifier = Identity()

           self.fc0 = nn.Linear(512, 512)

       else:

           self.feature_extraction = papy_s_net_branch(num_classes=10)  # ! remove dropout, add num_classes
           self.feature_extraction.classifier = nn.Sequential()  # !  remove the classifier

           self.fc0 = nn.Linear(512 * 3 * 3, 512)  # ! change 32768 to 512 * 3 * 3

       self.relu = nn.ReLU()

       self.fc1 = nn.Linear(512, 512)

       self.fc2 = nn.Linear(512, 1)

       self.sigmoid = nn.Sigmoid()

   def forward(self, x):

       x_l, x_r = x[0], x[1]

       left_embeddings = self.feature_extraction(x_l)

       right_embeddings = self.feature_extraction(x_r)

       Distance = torch.abs(torch.subtract(left_embeddings, right_embeddings))

       x = self.relu(self.fc0(Distance))

       x = self.relu(self.fc1(x))

       prediction = self.sigmoid(self.fc2(x))

       return prediction
net = Siamese("papy")
input = (torch.randn(1, 3, 224, 224), torch.randn(1, 3, 224, 224))
print(net(input))
