import csv
import imghdr
from importlib.resources import path
from operator import index
from pydoc import Helper
import random
from tkinter import N
from cv2 import imread, normalize, resize
from matplotlib import transforms
from numpy import size
import torch
from torch.utils.data import Dataset
from torchvision import datasets
from torchvision.transforms import ToTensor
import matplotlib.pyplot as plt
import pandas
import numpy
import os
from torchvision.io import read_image
from torch.utils.data import DataLoader
from PIL import Image
import torchvision.transforms.functional 
from socket import socket
from random import sample
import argparse
import datetime
import io
import os
import pickle

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import sklearn.metrics
import torch
import torch.nn.functional as F
import torch.utils.data as data
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
from torchvision.utils import make_grid
from torchvision.transforms import Resize, Compose

# Step 2: Prepare the dataset

class Data_set_Papy(Dataset):
    def __init__(self , csv_file   ,root_directory_image , transform=None , target_transform=None , train= True):
  
        self.annotations=pandas.read_csv(csv_file)
        self.root_directory_image=root_directory_image
        self.transform=transform 
        self.target_transform=target_transform
        self.train=train


    def __len__(self):
        return len(self.annotations) # return the lenght of our csv file 



    
    def __getitem__(self, index) : # returnin a sprecific image and correspanding target to that image 
        our_image_path= os.path.join( self.root_directory_image, self.annotations.iloc[index , 0 ]) # colom zero the first column with a random index
        image= imread(our_image_path)
        y_label= torch.tensor(int(self.annotations.iloc[index , 1 ]))

        if self.transform:
            image=self.transform(image)
        if self.target_transform:
            image=self.target_transform(image)

        return image , y_label

my_transforms = Compose( [ transforms.ToTensor(), Resize(224), ])

training_data = Data_set_Papy(
    
    csv_file='index_papyrus.csv',
    root_directory_image='input_frags_papyrus', 
    transform= my_transforms,
    target_transform=None,
    train=True

)
testing_data = Data_set_Papy(
    csv_file='index_papyrus.csv',
    root_directory_image='input_frags_papyrus',
    
    transform= my_transforms,
    target_transform=None,
    train=False

)
 # just making sure that my images are tensors in train data 
print("training data")
for img, label in training_data:
    print(label, type(img))
  # just making sure that my images are tensors in test  data  
print("testing data")
for img, label in testing_data:
    print(label, type(img))



fig, axes = plt.subplots(nrows=3, ncols=3)  # set up a 3x3 grid to visualize
image_list = [transforms.ToPILImage()(x[0]) for x in training_data]
for img, ax in zip(image_list, axes.ravel()):
    ax.imshow(img)
    
plt.gcf().set_size_inches(10, 10)
plt.show()
# donc la y'a une omage qui se repete  donc forcement  jai 2 image identique car dans mon dossier c le cas  
