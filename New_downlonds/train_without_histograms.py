import argparse
from calendar import EPOCH, c
import datetime
now = datetime.datetime.now()
import io
import os
import pickle
from pyexpat import model
from pyamg import test
import torch.optim as optim
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import sklearn.metrics
import torch
import torch.nn.functional as F
import torch.utils.data as data
from torchvision.transforms import Resize, Compose

from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
import argparse
import batch_utils
import utils
import data_set
from models_new_version import Siamese






# === Training Hyperparameters ===
batch_size = 128
nb_epochs = 1000
learning_rate = 1e-6  # overriden if the LearningRateScheduler is active
network_type = "resnet50"


device = torch.device('cuda:1' if torch.cuda.is_available() else 'cpu')

def compute_loss_accuracy(model, loader):
    loss = 0
    accuracy = 0
    totalinputs = 0
    totaloutput = 0
    batch_iter = iter(loader)
    for i in range(len(loader)):
        pairs, targets = next(batch_iter)

        inputs = pairs
        labels = targets

        output = model(inputs)
        # calculate loss between predict and target using binary cross entropy
        loss += F.binary_cross_entropy(output, labels.view(-1, 1)).item()
        # accuracy += torch.sum(labels == torch.argmax(output, dim=1)).item()
        accuracy += torch.count_nonzero(labels.view(-1, 1) == (output > 0.5)).item()

        totalinputs += len(inputs)
        totaloutput += len(output)

    loss /= totalinputs
    accuracy /= totaloutput

    return loss, accuracy
my_transforms = Compose( [ transforms.ToTensor(), transforms.Resize((224, 244)), ])
def collate_fn(batch):
   batch = list(filter(lambda x: x is not None, batch))
   return torch.utils.data.dataloader.default_collate(batch) 

net=Siamese(network_type)
input = (torch.randn(1, 3, 224, 224), torch.randn(1, 3, 224, 224))
#net(input)
optimiser=optim.Adam(net.parameters(), learning_rate)
totalLoss = 0
totalAccuracy = 0
totalOutputLen = 0


currentEpoch = 0

training_data = data_set.Data_set_Papy(
    
    csv_file='index_papyrus.csv',
    root_directory_image='input_frags_papyrus', 
    transform= my_transforms,
    target_transform=None,
    train=True

)
testing_data =data_set.Data_set_Papy(
    csv_file='index_papyrus.csv',
    root_directory_image='input_frags_papyrus',
    transform= my_transforms,
    target_transform=None,
    train=False

)



training_set=data_set.DataLoader(training_data , shuffle=True , batch_size=128 , collate_fn=collate_fn )
testing_set=data_set.DataLoader(testing_data, shuffle=True  ,batch_size=128 , collate_fn=collate_fn  )

for epoch in range(nb_epochs):
    # set train mode for model
    net.train()
    # get data from dataloader
    batch_iter = iter(training_set)
    pairs, targets = next(batch_iter)
    optimiser.zero_grad()

    # put data to device
    #pairs, targets = pairs.to(device), targets.to(device, dtype=torch.float32)
    # feed data to model
    #print("heloooooooooooooooooooo")
    preds = net(pairs)
    print("hellox ")
    # get loss value
    loss = F.binary_cross_entropy(preds, targets.view(-1, 1)) # we usede the function of binary to calculate the loste becs our preds have a value betwen zero or 1
    print('loss', loss.item())
    # backward gradient
    loss.backward()
    optimiser.step()
    totalLoss += loss.item()
    totalAccuracy += torch.count_nonzero(targets.view(-1, 1) == (preds > 0.5)).item()
    totalOutputLen += len(preds)
    if np.mod(epoch, 5) == 0 and epoch > 0:
        net.eval()
        with torch.no_grad():
            tr_loss, tr_accuracy = totalLoss / (epoch + 1), totalAccuracy / totalOutputLen
            va_loss, va_accuracy = compute_loss_accuracy(net, testing_set)

        print("Epoch: {}/{}.. ".format(epoch + 1, nb_epochs),
              "Training Loss: {:.3f}.. ".format(tr_loss),
              "Validiton Loss: {:.3f}.. ".format(va_loss),
              "Training Accuracy: {:.3f}".format(tr_accuracy),
              "Validation Accuracy: {:.3f}".format(va_accuracy))


